#!/bin/bash

# --- User Defined Fields ---
# <UDF name="hostname" label="Hostname" default="blog" />
# <UDF name="username" label="Username" default= "brentwg" />
# <UDF name="ssh_pub_key" label="SSH Pub Key" />

# --- ENV and Script variables ---
export DEBIAN_FRONTEND=noninteractive
NOW=$(date +'%Y-%m-%d_%H%M%S')
LOG="/root/StackScript_${NOW}.log"


# --- Create log ---
touch "${LOG}"
echo -e "StackScript executed: ${NOW}\n\n" >>"${LOG}" 2>&1

# --- Fixing the hostname ---
echo -e "\n\nFixing the hostname\n\n" >>"${LOG}" 2>&1
hostnamectl set-hostname $HOSTNAME
echo "127.0.1.1       ${HOSTNAME}" >> /etc/hosts
hostnamectl >>"${LOG}" 2>&1
cat /etc/hosts >>"${LOG}" 2>&1

# --- Install Ansible ---
echo -e "\n\nInstalling Ansible\n\n" >>"${LOG}" 2>&1
apt-get update >>"${LOG}" 2>&1
apt-get -yqq install ansible >>"${LOG}" 2>&1
# --- Create Ansible inventory file ---
ansible_hosts_file="/etc/ansible/hosts"
if [ -f "${ansible_hosts_file}" ]; then
  rm -f ${ansible_hosts_file}
fi
echo "localhost              ansible_connection=local" | tee "${ansible_hosts_file}"

# --- Install Ansible roles ---
echo -e "\n\nInstalling Ansible Roles\n\n" >>"${LOG}" 2>&1
ansible_roles=(
  "geerlingguy.docker"
)
for role in "${ansible_roles[@]}"
do
  ansible-galaxy install --force "${role}" >>"${LOG}" 2>&1
done

# --- Write Ansible playbook ---
echo -e "\n\nCreating Ansible Playbook\n\n" >>"${LOG}" 2>&1
ansible_playbook="/tmp/playbook.yaml"
cat <<-EOF > "${ansible_playbook}"
- hosts: all
  roles:
    - geerlingguy.docker

  tasks:
    - name: Add the default admin user
      user:
        name: $USERNAME
        groups: adm,sudo,docker
        append: yes

    - name: Add admin user to sudoers
      copy:
        dest: /etc/sudoers.d/$USERNAME
        content: "$USERNAME ALL=(ALL)  NOPASSWD: ALL"
    
    - name: Deploy SSH Pub key
      authorized_key:
        user: $USERNAME
        key: $SSH_PUB_KEY
        state: present

    - name: Disable Password Authentication
      lineinfile:
        dest=/etc/ssh/sshd_config
        regexp='^PasswordAuthentication'
        line="PasswordAuthentication no"
        state=present
        backup=yes
      notify:
        - restart ssh
 
    - name: Disable Root Login
      lineinfile:
        dest=/etc/ssh/sshd_config
        regexp='^PermitRootLogin'
        line="PermitRootLogin no"
        state=present
        backup=yes
      notify:
        - restart ssh
 
  handlers:
    - name: restart ssh
      service:
        name=sshd
        state=restarted
EOF

# --- Execute Ansible playbook ---
echo -e "\n\nExecuting Ansible Playbook\n\n" >>"${LOG}" 2>&1
ansible-playbook -i "${ansible_hosts_file}" "${ansible_playbook}" >>"${LOG}" 2>&1
