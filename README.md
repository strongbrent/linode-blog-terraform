# Linode: Blog Application Infrastructure (Terraform)

## Brief Description

The purpose of this repository is to store everything that's required to automate deployment of all the cloud infrastructure used to manage a small scale, personal (web) blogging application that's hosted in [Linode](https://www.linode.com/).

The application itself will be created from scratching using a Python web application framwork such as [FastAPI](https://fastapi.tiangolo.com/). The code for that project will be stored in a separate GitLab repository.

## Very Basic Project Requirements
- infrastructure as code (uses [Terraform](https://www.terraform.io/))
- starts out as an inexpensive, single-instance application but is capable of eventually scaling to a high availability or clustered solution
- instance is capable of hosting both the web application and backend database as a group of containers (installs docker via [Linode StackScripts](https://www.linode.com/docs/guides/platform/stackscripts/))
- deployed via [Terraform Cloud](https://www.terraform.io/cloud) (with the state file hosted there as well)

## TODO
- build a script to deploy the containerized application (once it's finished)
  - for the scale of this (single node) project, [Docker Compose](https://docs.docker.com/compose/) is sufficient
- incorporate DNS entry resources for my domain