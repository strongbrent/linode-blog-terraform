locals {
  inst_count  = 1
  inst_label  = "blog-1"
  inst_image  = "linode/ubuntu20.04"
  inst_region = "ca-central"
  inst_type   = "g6-nanode-1"

  inst_stackscript_id = "935509"
}

resource "linode_instance" "blog" {
  count           = local.inst_count
  label           = local.inst_label
  image           = local.inst_image
  region          = local.inst_region
  type            = local.inst_type
  authorized_keys = [data.linode_sshkey.blog.ssh_key]
  private_ip      = true

  stackscript_id = local.inst_stackscript_id
  stackscript_data = {
    "hostname"    = local.inst_label
    "username"    = data.linode_user.admin.username
    "ssh_pub_key" = data.linode_sshkey.blog.ssh_key
  }
}
